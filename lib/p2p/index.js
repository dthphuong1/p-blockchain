const net = require('net')

const wrtc = require('wrtc')
const Exchange = require('peer-exchange')

const Messages = require('./messages')
const {
  REQUEST_LATEST_BLOCK,
  RECEIVE_LATEST_BLOCK,
  REQUEST_BLOCKCHAIN,
  RECEIVE_BLOCKCHAIN,
} = require('./messages/message-type')
const blockchain = require('../Blockchain')
const logger = require('../cli/util/logger')

const p2p = new Exchange(process.env.CHAIN_NAME, { wrtc: wrtc })

class PeerToPeer {
  constructor() {
    this.peers = []
  }

  startServer(port) {
    const server = net
      .createServer((socket) =>
        p2p.accept(socket, (err, connection) => {
          if (err) {
            logger.log(`❗  ${err}`)
          } else {
            logger.log('👥  A new peer has connected to the FPO chain!')
            this.initConnection.call(this, connection)
          }
        })
      )
      .listen(port)
    logger.log(`📡  listening to peers on ${server.address().address}:${server.address().port}... `)
  }

  connectToPeer(host, port) {
    const socket = net.connect(port, host, () =>
      p2p.connect(socket, (err, connection) => {
        if (err) {
          logger.log(`❗  ${err}`)
        } else {
          logger.log('👥  Successfully connected to a new peer!')
          this.initConnection.call(this, connection)
        }
      })
    )
  }

  discoverPeers() {
    p2p.getNewPeer((err) => {
      if (err) {
        logger.log(`❗  ${err}`)
      } else {
        logger.log('👀  Discovered new peers.') //todo
      }
    })
  }

  closeConnection() {
    p2p.close((err) => {
      logger.log(`❗  ${err}`)
    })
  }

  broadcastLatest() {
    this.broadcast(Messages.sendLatestBlock(blockchain))
  }

  broadcast(message) {
    this.peers.forEach((peer) => this.write(peer, message))
  }

  write(peer, message) {
    peer.write(JSON.stringify(message))
  }

  initConnection(connection) {
    this.peers.push(connection)
    this.initMessageHandler(connection)
    this.initErrorHandler(connection)
    this.write(connection, Messages.getLatestBlock())
  }

  initMessageHandler(connection) {
    connection.on('data', (data) => {
      const message = JSON.parse(data.toString('utf8'))
      this.handleMessage(connection, message)
    })
  }

  initErrorHandler(connection) {
    connection.on('error', (error) => logger.log(`❗  ${error}`))
  }

  handleMessage(peer, message) {
    switch (message.type) {
      case REQUEST_LATEST_BLOCK:
        logger.log(`⬇  Peer requested for latest block.`)
        this.write(peer, Messages.sendLatestBlock(blockchain))
        break
      case REQUEST_BLOCKCHAIN:
        logger.log(`⬇  Peer requested for blockchain.`)
        this.write(peer, Messages.sendBlockchain(blockchain))
        break
      case RECEIVE_LATEST_BLOCK:
      case RECEIVE_BLOCKCHAIN:
        this.handleBlockchainResponse(message)
        break
      default:
        logger.log(`❓  Received unknown message type ${message.type}`)
    }
  }

  handleBlockchainResponse(message) {
    const receivedBlocks = JSON.parse(message.data).sort((b1, b2) => b1.index - b2.index)
    const latestBlockReceived = receivedBlocks[receivedBlocks.length - 1]
    const latestBlockHeld = blockchain.latestBlock

    const blockOrChain = receivedBlocks.length === 1 ? 'single block' : 'blockchain'
    logger.log(`⬇  Peer sent over ${blockOrChain}.`)

    if (latestBlockReceived.index <= latestBlockHeld.index) {
      logger.log(`💤  Received latest block is not longer than current blockchain. Do nothing`)
      return null
    }

    logger.log(
      `🐢  Blockchain possibly behind. Received latest block is #${latestBlockReceived.index}. Current latest block is #${latestBlockHeld.index}.`
    )
    if (latestBlockHeld.hash === latestBlockReceived.previousHash) {
      logger.log(`👍  Previous hash received is equal to current hash. Append received block to blockchain.`)
      blockchain.addBlockFromPeer(latestBlockReceived)
      this.broadcast(Messages.sendLatestBlock(blockchain))
    } else if (receivedBlocks.length === 1) {
      logger.log(`🤔  Received previous hash different from current hash. Get entire blockchain from peer.`)
      this.broadcast(Messages.getBlockchain())
    } else {
      logger.log(`⛓  Peer blockchain is longer than current blockchain.`)
      blockchain.replaceChain(receivedBlocks)
      this.broadcast(Messages.sendLatestBlock(blockchain))
    }
  }
}

module.exports = new PeerToPeer()
