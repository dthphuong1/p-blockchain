const { REQUEST_LATEST_BLOCK, RECEIVE_LATEST_BLOCK, REQUEST_BLOCKCHAIN, RECEIVE_BLOCKCHAIN } = require('./message-type')
const logger = require('../../cli/util/logger')

class Messages {
  getLatestBlock() {
    logger.log('⬆  Asking peer for latest block')
    return {
      type: REQUEST_LATEST_BLOCK,
    }
  }

  sendLatestBlock(blockchain) {
    logger.log('⬆  Sending peer latest block')
    return {
      type: RECEIVE_LATEST_BLOCK,
      data: JSON.stringify([blockchain.latestBlock]),
    }
  }

  getBlockchain() {
    logger.log('⬆  Asking peer for entire blockchain')
    return {
      type: REQUEST_BLOCKCHAIN,
    }
  }

  sendBlockchain(blockchain) {
    logger.log('⬆  Sending peer entire blockchain')
    return {
      type: RECEIVE_BLOCKCHAIN,
      data: JSON.stringify(blockchain.get()),
    }
  }
}

module.exports = new Messages()
