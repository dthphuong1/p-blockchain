const vorpal = require('vorpal')()

module.exports = function (vorpal) {
  vorpal
    .use(require('./command/open-connection'))
    .use(require('./command/connect-peer.js'))
    .use(require('./command/list-peers.js'))
    .use(require('./command/discover-peers.js'))
    .use(require('./command/list-blockchain.js'))
    .use(require('./command/mine-block.js'))
    .use(require('./util/welcome.js'))
    .delimiter('fpo-chain →')
    .show()
}
