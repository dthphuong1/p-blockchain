const fpochain = require('../../Blockchain')
const logBlockchain = require('../util/table')

module.exports = function (vorpal) {
  vorpal
    .command('blockchain', 'See the current state of the blockchain.')
    .alias('bc')
    .action(function (args, callback) {
      logBlockchain(fpochain.blockchain)
      callback()
    })
}
