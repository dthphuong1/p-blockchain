const vorpal = require('vorpal')()
const cliSpinners = require('cli-spinners')

module.exports = (function () {
  let index = 0

  function draw() {
    const sequence = cliSpinners.dots8.frames
    index++
    index = index < sequence.length - 1 ? index + 1 : 0

    vorpal.ui.redraw(`${sequence[index]} Mining new block`)
  }

  function clear() {
    vorpal.ui.redraw.done()
    vorpal.ui.redraw.clear()
  }

  return {
    draw: draw,
    clear: clear,
  }
})()
