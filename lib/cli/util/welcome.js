const logger = require('./logger')
const vorpal = require('vorpal')()

module.exports = function (vorpal) {
  logger.log(`👋  Welcome to ${process.env.CHAIN_NAME} CLI !`)

  vorpal.exec('help')
}
