const crypto = require('crypto-js')

const Block = require('./Block')
const logger = require('./cli/util/logger')
const spinner = require('./cli/util/spinner')

class Blockchain {
  constructor() {
    this.blockchain = [Block.genesis]
    this.difficulty = parseInt(process.env.DIFFICULTY)
  }

  get() {
    return this.blockchain
  }

  get latestBlock() {
    return this.blockchain[this.blockchain.length - 1]
  }

  mine(data) {
    const newBlock = this.generateNextBlock(data)
    if (this.addBlock(newBlock)) {
      logger.log('🎉  Congratulations! A new block was mined. 💎')
    }
  }

  replaceChain(newChain) {
    if (!this.isValidChain(newChain)) {
      logger.log("❌  Replacement chain is not valid. Won't replace existing blockchain.")
      return null
    }

    if (newChain.length <= this.blockchain.length) {
      logger.log("❌  Replacement chain is shorter than original. Won't replace existing blockchain.")
      return null
    }

    logger.log('✅  Received blockchain is valid. Replacing current blockchain with received blockchain')
    this.blockchain = newChain.map(
      (json) => new Block(json.index, json.previousHash, json.timestamp, json.data, json.hash, json.nonce)
    )
  }

  generateNextBlock(data) {
    const nextIndex = this.latestBlock.index + 1
    const previousHash = this.latestBlock.hash
    let nextTimestamp = new Date().getTime()
    let nonce = 0
    let nextHash = this.calculateHash(nextIndex, previousHash, nextTimestamp, data, nonce)

    while (!this.isValidHashDifficulty(nextHash)) {
      nonce = nonce + 1
      nextTimestamp = new Date().getTime()
      nextHash = this.calculateHash(nextIndex, previousHash, nextTimestamp, data, nonce)
      spinner.draw('moon')
    }
    spinner.clear()

    const nextBlock = new Block(nextIndex, previousHash, nextTimestamp, data, nextHash, nonce)

    return nextBlock
  }

  addBlock(newBlock) {
    if (this.isValidNewBlock(newBlock, this.latestBlock)) {
      this.blockchain.push(newBlock)
      return true
    } else {
      return false
    }
  }

  addBlockFromPeer(json) {
    if (this.isValidNewBlock(json, this.latestBlock)) {
      this.blockchain.push(new Block(json.index, json.previousHash, json.timestamp, json.data, json.hash, json.nonce))
    }
  }

  // ========== Calculate and Valid method ==========
  calculateHashForBlock(block) {
    const { index, previousHash, timestamp, data, nonce } = block
    return this.calculateHash(index, previousHash, timestamp, data, nonce)
  }

  calculateHash(index, previousHash, timestamp, data, nonce) {
    return crypto.SHA256(index + previousHash + timestamp + JSON.stringify(data) + nonce).toString()
  }

  isValidHashDifficulty(hash) {
    return hash.startsWith('0'.repeat(this.difficulty))
  }

  isValidNewBlock(newBlock, previousBlock) {
    const blockHash = this.calculateHashForBlock(newBlock)

    if (previousBlock.index + 1 !== newBlock.index) {
      logger.log('❌  new block has invalid index')
      return false
    } else if (previousBlock.hash !== newBlock.previousHash) {
      logger.log('❌  new block has invalid previous hash')
      return false
    } else if (blockHash !== newBlock.hash) {
      logger.log(`❌  invalid hash: ${blockHash} ${newBlock.hash}`)
      return false
    } else if (!this.isValidHashDifficulty(this.calculateHashForBlock(newBlock))) {
      logger.log(`❌  invalid hash does not meet difficulty requirements: ${this.calculateHashForBlock(newBlock)}`)
      return false
    }
    return true
  }

  isValidChain(chain) {
    if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis)) {
      return false
    }

    const tempChain = [chain[0]]
    for (let i = 1; i < chain.length; i = i + 1) {
      if (this.isValidNewBlock(chain[i], tempChain[i - 1])) {
        tempChain.push(chain[i])
      } else {
        return false
      }
    }
    return true
  }

  isChainLonger(chain) {
    return chain.length > this.blockchain.length
  }
}

module.exports = new Blockchain()
