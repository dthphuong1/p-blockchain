module.exports = class Block {
  constructor(index = 0, previousHash = '0', timestamp = new Date().getTime(), data = 'none', hash = '', nonce = 0) {
    this.index = index
    this.previousHash = previousHash.toString()
    this.timestamp = timestamp
    this.data = data
    this.hash = hash.toString()
    this.nonce = nonce
  }

  static get genesis() {
    return new Block(
      0,
      '0',
      1638029147951,
      'Welcome to FPO Chain - a product of FPO Co.,Ltd in Vietnam',
      process.env.GENESIS_HASH,
      process.env.GENESIS_NONCE
    )
  }
}
