#!/usr/bin/env node
require('dotenv').config()

const vorpal = require('vorpal')()
const p2p = require('./lib/p2p')
vorpal.use(require('./lib/cli'))

switch (process.env.ENV) {
  case 'DEVELOPMENT':
    break
  case 'PRODUCTION':
    p2p.startServer(parseInt(process.env.PORT))
    break
}
